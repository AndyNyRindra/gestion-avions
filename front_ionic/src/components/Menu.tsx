import { IonMenu, IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, IonLabel } from '@ionic/react'
import React, { useEffect } from 'react'

type Props = {}

const Menu = (props: Props) => {
  const handleLogout = () => {
      var token = sessionStorage.getItem('user_token');
      const headers = new Headers();
      if (token != null) {
        headers.set('user_token', token);
      }
      fetch('https://avions-production-1b66.up.railway.app/users/token/disable', {
        method: 'GET',
        headers: headers
    })
         .then((response) => response.json())
         .then((data) => {
            console.log(data);
            if (data.data != null) {
              sessionStorage.removeItem("user_token")
              window.location.replace("/login")
            }
         })
         .catch((err) => {
            console.log(err.message);
         });
    
  };
    return (
        <IonMenu contentId="main-content">
            <IonContent className="ion-padding">
              <IonList>
                <IonItem onClick={handleLogout}>
                  <IonLabel>Logout</IonLabel>
                </IonItem>
                <IonItem href={"/vehicules"}>
                  <IonLabel>Vehicules</IonLabel>
                </IonItem>
                <IonItem href={"/vehicules/assurance/1"}>
                  <IonLabel>Expiré dans 1 mois</IonLabel>
                </IonItem>
                <IonItem href={"/vehicules/assurance/3"}>
                  <IonLabel>Expiré dans 3 mois</IonLabel>
                </IonItem>
              </IonList>
            </IonContent>
        </IonMenu>
      );
}

export default Menu