import { IonButton, IonCheckbox, IonInput, IonItem, IonLabel, useIonAlert } from '@ionic/react'
import React, { useRef } from 'react'
import { useHistory } from 'react-router';
import '../assets/myappcss/Form.css';
type Props = {}

const Form = (props: Props) => {
    const history = useHistory();
    var usernameRef = useRef<HTMLIonInputElement>(null);
    var passwordRef = useRef<HTMLIonInputElement>(null);
    const [presentAlert] = useIonAlert();
    function getUserData() {
        return [{ userid: 1, username: 'ranja', password: 'ranja123' }, { userid: 2, username: 'ranja2', password: 'ranja1234' }, { userid: 3, username: 'ranja3', password: 'ranja12345' }];
    }
    function authentificate() {
        var username = usernameRef.current?.value;
        var password = passwordRef.current?.value;
        var user = getUserData().find(x => x.username == username && x.password == password);
        if (user) {
            sessionStorage.setItem("user",JSON.stringify(user));
            history.push('/home');
        } else {
            presentAlert({
                header: 'Sign in failed',
                message: 'Invalid username or password',
                buttons: ['OK'],
            })
        }
    }
    return (
        <form className="ion-padding myform">
            <IonLabel className='formtitle'>Login Form</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Username</IonLabel>
                <IonInput ref={usernameRef} />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput ref={passwordRef} type="password" />
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={authentificate}>
                Login
            </IonButton>
        </form>
    )
}

export default Form