import { IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle } from '@ionic/react'
import React from 'react'

type Props = {
    label: string
}

const MenuHeader = (props: Props) => {
    return (
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonMenuButton></IonMenuButton>
                </IonButtons>
                <IonTitle>{props.label}</IonTitle>
            </IonToolbar>
        </IonHeader>
    )
}

export default MenuHeader