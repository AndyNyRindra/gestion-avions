import { IonHeader, IonToolbar, IonButtons, IonMenuButton, IonTitle, IonCol, IonContent, IonGrid, IonPage, IonRow } from '@ionic/react'
import { Vehicule } from '../models/Vehicule'
import VehiculeItem from '../pages/VehiculeItem';
import Footer from './Footer';
import Menu from './Menu';
import MenuHeader from './MenuHeader';

type Props = {
    vehicules: Vehicule[]
}

const ListeVehicule = (props: Props) => {
    return (
        <><Menu /><IonPage id="main-content">
  
          <MenuHeader label='Avions' />
          <IonContent fullscreen={true}>
            <IonHeader collapse="condense">
              <IonToolbar>
                <IonTitle size="large">Vehicules</IonTitle>
              </IonToolbar>
            </IonHeader>
  
            <IonGrid fixed>
              <IonRow>
                {props.vehicules.map((vehicule: Vehicule) => (
                  <IonCol size="12" size-md="6" key={vehicule.id}>
                    <VehiculeItem
                      key={vehicule.id}
                      vehicule={vehicule} />
                  </IonCol>
                ))}
              </IonRow>
            </IonGrid>
          </IonContent>
          <Footer />
        </IonPage></>
                
             
          );
       };


export default ListeVehicule