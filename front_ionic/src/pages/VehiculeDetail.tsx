import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent, useIonAlert, IonCol, IonGrid, IonRow } from "@ionic/react";
import { useEffect, useState } from "react";
import { Redirect, RouteComponentProps } from "react-router";
import Footer from "../components/Footer";
import Menu from "../components/Menu";
import MenuHeader from "../components/MenuHeader";
import { Kilometrage } from "../models/Kilometrage";
import { Vehicule } from "../models/Vehicule";
import KilometrageItem from "./KilometrageItem";
import VehiculeItem from "./VehiculeItem";
import checkToken from "../data/CheckToken";

interface UserDetailPageProps
    extends RouteComponentProps<{
        id: string;
    }> { }

const UserDetailPage: React.FC<UserDetailPageProps> =  ({ match }) => {
    const [vehicule, setVehicule] = useState<Vehicule>();
    const [kilometrages, setKilometrages] = useState([]);
    const [token, setToken] = useState<Boolean>();
    const [presentAlert] = useIonAlert();
    useEffect(() => {

        async function fetchToken() {
            const token = await checkToken();
            setToken(token);
          }
        
          fetchToken();

        fetch('https://avions-production-1b66.up.railway.app/avions/' + match.params.id)
            .then((response) => response.json())
            .then((data) => {
                if (data.data == null) {
                    presentAlert({
                        header: data.error.code,
                        message: data.error.message,
                        buttons: ['OK'],
                    })
                } else {
                    console.log(data);
                    setVehicule(data.data);
                }

            })
            .catch((err) => {
                console.log(err.message);
            });

        fetch('https://avions-production-1b66.up.railway.app/avions/' + match.params.id + '/kilometrages')
            .then((response) => response.json())
            .then((data) => {
                if (data.data == null) {
                    presentAlert({
                        header: data.error.code,
                        message: data.error.message,
                        buttons: ['OK'],
                    })
                } else {
                    console.log(data);
                    setKilometrages(data.data);
                }

            })
            .catch((err) => {
                console.log(err.message);
            });


    }, []);
    
    if (token === false) {
        return <Redirect to="/login" />;
    }
    return (
        <><Menu /><IonPage id="main-content">

            <MenuHeader label='Vehicules' />
            <IonContent fullscreen={true}>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Vehicules</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <VehiculeItem
                    key={vehicule?.id}
                    vehicule={vehicule} />
                <IonGrid fixed>

                    <IonRow>
                        {kilometrages.map((kilometrage: Kilometrage) => (
                            <IonCol size="12" size-md="6" key={kilometrage.id}>
                                <KilometrageItem
                                    key={kilometrage.id}
                                    kilometrage={kilometrage} />
                            </IonCol>
                        ))}
                    </IonRow>
                </IonGrid>
            </IonContent>
            <Footer />
        </IonPage></>
    );
}



export default UserDetailPage;