import  { useState, useEffect } from 'react';
import { Vehicule } from '../models/Vehicule';

import ListeVehicule from '../components/ListeVehicule';
import { RouteComponentProps } from 'react-router';

interface UserDetailPageProps
    extends RouteComponentProps<{
        mois: string;
    }> { }

const VehiculeAssurance: React.FC<UserDetailPageProps> = ({ match }) => {
    const [vehicules, setVehicules] = useState<Vehicule[]>([]);

    useEffect(() => {
       fetch('https://avions-production-1b66.up.railway.app/avions/assurance/' + match.params.mois)
          .then((response) => response.json())
          .then((data) => {
             console.log(data);
             setVehicules(data.data);
          })
          .catch((err) => {
             console.log(err.message);
          });
    }, []);

    return (
      <ListeVehicule vehicules={vehicules}/>
              
           
        );
     };

     export default VehiculeAssurance;