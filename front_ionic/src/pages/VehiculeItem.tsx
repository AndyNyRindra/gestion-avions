import React from 'react';
import { Vehicule } from '../models/Vehicule' 
import { IonCard, IonCardHeader, IonItem, IonLabel, IonAvatar, IonCardContent, IonList } from '@ionic/react';

interface VehiculeItemProps {
  vehicule?: Vehicule;
}

const VehiculeItem: React.FC<VehiculeItemProps> = ({vehicule}) => {
  return (
    <>
      <IonCard className="speaker-card">
        <IonCardHeader>
          <IonItem button detail={false} lines="none" className="speaker-item" href={`/vehicules/${vehicule?.id}`}>
            <IonLabel>
              <h2>{vehicule?.matricule}</h2>
              <p>{vehicule?.marque.nom}</p>
            </IonLabel>
          </IonItem>
        </IonCardHeader>

    
      </IonCard>
    </>
  );
};

export default VehiculeItem;