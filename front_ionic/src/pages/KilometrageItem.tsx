import React from 'react';
import { Kilometrage } from '../models/Kilometrage'
import { IonCard, IonCardHeader, IonItem, IonLabel, IonAvatar, IonCardContent, IonList, IonDatetime } from '@ionic/react';

interface KilometrageItemProps {
    kilometrage?: Kilometrage;
}

const KilometrageItem: React.FC<KilometrageItemProps> = ({ kilometrage }) => {
    return (
        <>
            <IonCard className="speaker-card">
                <IonCardHeader>
                    <IonItem button detail={false} lines="none" className="speaker-item" routerLink={`/vehicules/${kilometrage?.id}`}>
                        <IonLabel>
                            <h2>Date</h2>
                            <p>{kilometrage?.date}</p>
                            <h2>Kilometre debut</h2>
                            <p>{kilometrage?.km_debut}</p>
                            <h2>Kilometre fin</h2>
                            <p>{kilometrage?.km_fin}</p>
                        </IonLabel>
                    </IonItem>
                </IonCardHeader>


            </IonCard>
        </>
    );
};

export default KilometrageItem;