import { Vehicule } from "./Vehicule";

export interface Kilometrage {
    id: number;
    date: string;
    km_debut: number;
    km_fin: number;
    vehicule: Vehicule
}