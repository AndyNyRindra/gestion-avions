import { User } from "./User";

export interface Token {
    id: number;
    user: User;
    value: string;
    dateExpiration: string;
}