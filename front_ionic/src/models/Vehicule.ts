import { Marque } from "./Marque";

export interface Vehicule {
    id: number;
    matricule: string;
    marque: Marque;
}