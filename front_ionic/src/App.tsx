import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonButtons, IonContent, IonHeader, IonMenu, IonMenuButton, IonPage, IonRouterOutlet, IonTitle, IonToolbar, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Login from './pages/Login';
import Header from './components/Header';
import Footer from './components/Footer';
import './assets/myappcss/App.css';
import Home from './pages/Home';
import VehiculeList from './pages/VehiculeList';
import InfiniteScrollExample from './components/InfiniteScrollExample';
import VehiculeDetail from './pages/VehiculeDetail';
import VehiculeAssurance from './pages/VehiculeAssuranceExpi';
setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
      <Route path=""><Redirect from='/' to='/vehicules' /></Route>
        <Route path="/login" component={Login}></Route>
        <Route path="/home" component={Home}></Route>
        <Route path="/scroll" component={InfiniteScrollExample}></Route>
        <Route path="/vehicules" component={VehiculeList}></Route>
        <Route path="/vehicules/:id" component={VehiculeDetail}></Route>
        <Route path="/vehicules/assurance/:mois" component={VehiculeAssurance}></Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
