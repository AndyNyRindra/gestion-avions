--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: assurance; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.assurance (
    id integer NOT NULL,
    date_payement date DEFAULT CURRENT_DATE NOT NULL,
    date_expiration date NOT NULL,
    vehicule_id integer NOT NULL,
    CONSTRAINT cns_assurance CHECK ((date_payement < date_expiration))
);


ALTER TABLE public.assurance OWNER TO vehicule;

--
-- Name: assurance_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.assurance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assurance_id_seq OWNER TO vehicule;

--
-- Name: assurance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.assurance_id_seq OWNED BY public.assurance.id;


--
-- Name: kilometrage; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.kilometrage (
    id bigint NOT NULL,
    date date NOT NULL,
    km_debut double precision NOT NULL,
    km_fin double precision NOT NULL,
    vehicule_id bigint
);


ALTER TABLE public.kilometrage OWNER TO vehicule;

--
-- Name: kilometrage_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.kilometrage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kilometrage_id_seq OWNER TO vehicule;

--
-- Name: kilometrage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.kilometrage_id_seq OWNED BY public.kilometrage.id;


--
-- Name: marque; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.marque (
    id bigint NOT NULL,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.marque OWNER TO vehicule;

--
-- Name: marque_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.marque_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marque_id_seq OWNER TO vehicule;

--
-- Name: marque_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.marque_id_seq OWNED BY public.marque.id;


--
-- Name: user_token; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.user_token (
    id bigint NOT NULL,
    date_expiration timestamp without time zone NOT NULL,
    value character varying(255) NOT NULL,
    user_id bigint
);


ALTER TABLE public.user_token OWNER TO vehicule;

--
-- Name: user_token_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.user_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_token_id_seq OWNER TO vehicule;

--
-- Name: user_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.user_token_id_seq OWNED BY public.user_token.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO vehicule;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO vehicule;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: vehicule; Type: TABLE; Schema: public; Owner: vehicule
--

CREATE TABLE public.vehicule (
    id bigint NOT NULL,
    matricule character varying(255) NOT NULL,
    marque_id bigint NOT NULL
);


ALTER TABLE public.vehicule OWNER TO vehicule;

--
-- Name: vehicule_id_seq; Type: SEQUENCE; Schema: public; Owner: vehicule
--

CREATE SEQUENCE public.vehicule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehicule_id_seq OWNER TO vehicule;

--
-- Name: vehicule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: vehicule
--

ALTER SEQUENCE public.vehicule_id_seq OWNED BY public.vehicule.id;


--
-- Name: assurance id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.assurance ALTER COLUMN id SET DEFAULT nextval('public.assurance_id_seq'::regclass);


--
-- Name: kilometrage id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.kilometrage ALTER COLUMN id SET DEFAULT nextval('public.kilometrage_id_seq'::regclass);


--
-- Name: marque id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.marque ALTER COLUMN id SET DEFAULT nextval('public.marque_id_seq'::regclass);


--
-- Name: user_token id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.user_token ALTER COLUMN id SET DEFAULT nextval('public.user_token_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: vehicule id; Type: DEFAULT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.vehicule ALTER COLUMN id SET DEFAULT nextval('public.vehicule_id_seq'::regclass);


--
-- Data for Name: assurance; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.assurance (id, date_payement, date_expiration, vehicule_id) FROM stdin;
1	2022-05-20	2022-12-20	5
2	2022-06-21	2023-03-02	6
3	2022-08-21	2023-02-02	7
4	2022-02-12	2023-03-12	8
5	2022-02-12	2023-02-23	9
6	2022-02-12	2023-01-13	10
\.


--
-- Data for Name: kilometrage; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.kilometrage (id, date, km_debut, km_fin, vehicule_id) FROM stdin;
1	2022-02-02	5	10	5
2	2022-02-02	5	10	5
\.


--
-- Data for Name: marque; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.marque (id, nom) FROM stdin;
1	Toyota
2	Porsche
\.


--
-- Data for Name: user_token; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.user_token (id, date_expiration, value, user_id) FROM stdin;
1	2022-11-22 13:32:29.262	7da47d23b7bd86e540dc0dfdce47ce800380d1ec	2
2	2022-11-25 08:45:38.914629	a99bdef11b204cd739b9d4499940ff380760652c	2
3	2022-12-08 11:40:36.744	cd6a48d8d16066060695e130f87ea9b4ff84283b	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.users (id, email, password, username) FROM stdin;
2	admin@mail.com	1234	admin
\.


--
-- Data for Name: vehicule; Type: TABLE DATA; Schema: public; Owner: vehicule
--

COPY public.vehicule (id, matricule, marque_id) FROM stdin;
5	0000TBL	1
6	0000TBM	2
7	0000TBA	1
8	0000TAD	1
9	0000TAB	2
10	0000TED	2
\.


--
-- Name: assurance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.assurance_id_seq', 6, true);


--
-- Name: kilometrage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.kilometrage_id_seq', 2, true);


--
-- Name: marque_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.marque_id_seq', 2, true);


--
-- Name: user_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.user_token_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- Name: vehicule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: vehicule
--

SELECT pg_catalog.setval('public.vehicule_id_seq', 10, true);


--
-- Name: kilometrage kilometrage_pkey; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT kilometrage_pkey PRIMARY KEY (id);


--
-- Name: marque marque_pkey; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.marque
    ADD CONSTRAINT marque_pkey PRIMARY KEY (id);


--
-- Name: assurance pk_assurance; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT pk_assurance PRIMARY KEY (id);


--
-- Name: user_token user_token_pkey; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT user_token_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vehicule vehicule_pkey; Type: CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.vehicule
    ADD CONSTRAINT vehicule_pkey PRIMARY KEY (id);


--
-- Name: assurance fk_assurance_vehicule; Type: FK CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT fk_assurance_vehicule FOREIGN KEY (vehicule_id) REFERENCES public.vehicule(id);


--
-- Name: user_token fkfadr8tg4d19axmfe9fltvrmqt; Type: FK CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT fkfadr8tg4d19axmfe9fltvrmqt FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: kilometrage fkoxf50010aj8y70cr85iw5o1sc; Type: FK CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT fkoxf50010aj8y70cr85iw5o1sc FOREIGN KEY (vehicule_id) REFERENCES public.vehicule(id);


--
-- Name: vehicule fkvrx2xaooytr6h4qe4p9u0te8; Type: FK CONSTRAINT; Schema: public; Owner: vehicule
--

ALTER TABLE ONLY public.vehicule
    ADD CONSTRAINT fkvrx2xaooytr6h4qe4p9u0te8 FOREIGN KEY (marque_id) REFERENCES public.marque(id);


--
-- PostgreSQL database dump complete
--

