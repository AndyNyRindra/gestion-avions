--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: assurance; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.assurance (
    id integer NOT NULL,
    date_payement date DEFAULT CURRENT_DATE NOT NULL,
    date_expiration date NOT NULL,
    avion_id integer NOT NULL,
    CONSTRAINT cns_assurance CHECK ((date_payement < date_expiration))
);


ALTER TABLE public.assurance OWNER TO avion;

--
-- Name: assurance_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.assurance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assurance_id_seq OWNER TO avion;

--
-- Name: assurance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.assurance_id_seq OWNED BY public.assurance.id;


--
-- Name: avion; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.avion (
    id bigint NOT NULL,
    matricule character varying(255) NOT NULL,
    marque_id bigint NOT NULL
);


ALTER TABLE public.avion OWNER TO avion;

--
-- Name: kilometrage; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.kilometrage (
    id bigint NOT NULL,
    date date NOT NULL,
    km_debut double precision NOT NULL,
    km_fin double precision NOT NULL,
    avion_id bigint
);


ALTER TABLE public.kilometrage OWNER TO avion;

--
-- Name: kilometrage_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.kilometrage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kilometrage_id_seq OWNER TO avion;

--
-- Name: kilometrage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.kilometrage_id_seq OWNED BY public.kilometrage.id;


--
-- Name: marque; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.marque (
    id bigint NOT NULL,
    nom character varying(255) NOT NULL
);


ALTER TABLE public.marque OWNER TO avion;

--
-- Name: marque_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.marque_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marque_id_seq OWNER TO avion;

--
-- Name: marque_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.marque_id_seq OWNED BY public.marque.id;


--
-- Name: user_token; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.user_token (
    id bigint NOT NULL,
    date_expiration timestamp without time zone NOT NULL,
    value character varying(255) NOT NULL,
    user_id bigint
);


ALTER TABLE public.user_token OWNER TO avion;

--
-- Name: user_token_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.user_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_token_id_seq OWNER TO avion;

--
-- Name: user_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.user_token_id_seq OWNED BY public.user_token.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: avion
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO avion;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO avion;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: vehicule_id_seq; Type: SEQUENCE; Schema: public; Owner: avion
--

CREATE SEQUENCE public.vehicule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehicule_id_seq OWNER TO avion;

--
-- Name: vehicule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: avion
--

ALTER SEQUENCE public.vehicule_id_seq OWNED BY public.avion.id;


--
-- Name: assurance id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.assurance ALTER COLUMN id SET DEFAULT nextval('public.assurance_id_seq'::regclass);


--
-- Name: avion id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.avion ALTER COLUMN id SET DEFAULT nextval('public.vehicule_id_seq'::regclass);


--
-- Name: kilometrage id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.kilometrage ALTER COLUMN id SET DEFAULT nextval('public.kilometrage_id_seq'::regclass);


--
-- Name: marque id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.marque ALTER COLUMN id SET DEFAULT nextval('public.marque_id_seq'::regclass);


--
-- Name: user_token id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.user_token ALTER COLUMN id SET DEFAULT nextval('public.user_token_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: assurance; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.assurance (id, date_payement, date_expiration, avion_id) FROM stdin;
1	2022-05-20	2022-12-20	5
2	2022-06-21	2023-03-02	6
3	2022-08-21	2023-02-02	7
4	2022-02-12	2023-03-12	8
5	2022-02-12	2023-02-23	9
6	2022-02-12	2023-01-13	10
7	2021-12-01	2022-01-01	5
\.


--
-- Data for Name: avion; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.avion (id, matricule, marque_id) FROM stdin;
5	0000TBL	1
6	0000TBM	2
7	0000TBA	1
\.


--
-- Data for Name: kilometrage; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.kilometrage (id, date, km_debut, km_fin, avion_id) FROM stdin;
1	2022-02-02	5	10	5
2	2022-02-02	5	10	5
\.


--
-- Data for Name: marque; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.marque (id, nom) FROM stdin;
1	Toyota
2	Porsche
\.


--
-- Data for Name: user_token; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.user_token (id, date_expiration, value, user_id) FROM stdin;
3	2022-12-20 08:15:47.237086	95658c444108a3895af075924037b74937c01dad	2
4	2022-12-20 08:54:05.220814	f81aeff031cb3680caab6811cb2451c66baefeb0	2
5	2022-12-20 10:54:05.225	7c0325e963c27cd565a81077b906b878bbedc7eb	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: avion
--

COPY public.users (id, email, password, username) FROM stdin;
2	admin@mail.com	1234	admin
\.


--
-- Name: assurance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.assurance_id_seq', 8, true);


--
-- Name: kilometrage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.kilometrage_id_seq', 2, true);


--
-- Name: marque_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.marque_id_seq', 2, true);


--
-- Name: user_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.user_token_id_seq', 5, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- Name: vehicule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: avion
--

SELECT pg_catalog.setval('public.vehicule_id_seq', 10, true);


--
-- Name: kilometrage kilometrage_pkey; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT kilometrage_pkey PRIMARY KEY (id);


--
-- Name: marque marque_pkey; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.marque
    ADD CONSTRAINT marque_pkey PRIMARY KEY (id);


--
-- Name: assurance pk_assurance; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT pk_assurance PRIMARY KEY (id);


--
-- Name: user_token user_token_pkey; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT user_token_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: avion vehicule_pkey; Type: CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.avion
    ADD CONSTRAINT vehicule_pkey PRIMARY KEY (id);


--
-- Name: user_token fkfadr8tg4d19axmfe9fltvrmqt; Type: FK CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.user_token
    ADD CONSTRAINT fkfadr8tg4d19axmfe9fltvrmqt FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: kilometrage fkoxf50010aj8y70cr85iw5o1sc; Type: FK CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT fkoxf50010aj8y70cr85iw5o1sc FOREIGN KEY (avion_id) REFERENCES public.avion(id);


--
-- Name: avion fkvrx2xaooytr6h4qe4p9u0te8; Type: FK CONSTRAINT; Schema: public; Owner: avion
--

ALTER TABLE ONLY public.avion
    ADD CONSTRAINT fkvrx2xaooytr6h4qe4p9u0te8 FOREIGN KEY (marque_id) REFERENCES public.marque(id);


--
-- PostgreSQL database dump complete
--

